﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ShootScript : MonoBehaviour
{
    public SteamVR_Action_Boolean m_ShootTrigger = null;
    public SteamVR_Action_Boolean m_halfauto = null;
    public GameObject impactEffect;
    public GameObject bullet;
    public GameObject emtyBullet;
    public GameObject Explosion;
    public AudioClip emptySound;
    public AudioClip shootSound;
    public AudioClip rocketSound;
    public AudioClip upgradedShootSound; 

    private Interactable m_CurrentInteractable = null;
    private float nextTimeToFire = 0f;
    private float emtyBulletSpeed = 1.2f;
    private float dmg;
    private float impactForce;
    private float range;
    private float bulletSize;
    private float bulletSpeed;
    private float fireRate;
    private string activeWeapon;
    private GameObject bulletSpawnPoint;
    private GameObject emtyBulletSpawnPoint;
    private ParticleSystem flash;
    private bool triggerPressed = false;
    private bool fire = false;
    private bool doubleTap = false;

    void Update()
    {
        m_CurrentInteractable = GetComponent<Hand>().getGrabbeldObject();

        if (m_halfauto.GetStateDown(SteamVR_Input_Sources.RightHand)) triggerPressed = !triggerPressed;

        if (m_CurrentInteractable && m_CurrentInteractable.tag == "Weapon" && bullet && (m_ShootTrigger.GetStateDown(SteamVR_Input_Sources.RightHand) || fire) && Time.time >= nextTimeToFire)
        {
            if(triggerPressed) fire = true;
            if (transform.parent.parent.GetComponent<Player>().shoot(false))
            {
                changeWeaponStats();
                transform.parent.parent.GetComponent<Player>().shoot(true);
                shoot();
            }
            else
            {
                if(!GetComponent<AudioSource>().isPlaying)
                {
                    GetComponent<AudioSource>().PlayOneShot(emptySound);
                }
            }
        }

        if(m_ShootTrigger.GetStateUp(SteamVR_Input_Sources.RightHand) && triggerPressed) fire = false;
    }

    private void shoot ()
    {
        if (activeWeapon == "RPG7" || activeWeapon == "RPG7Upgrade")
        {
            GetComponent<AudioSource>().PlayOneShot(rocketSound);

            //Clone bullets
            GameObject rakete = transform.parent.Find("Weapon").GetChild(0).Find("Rakete").gameObject;
            rakete.GetComponent<Rigidbody>().isKinematic = false;
            rakete.SetActive(false);
            GameObject raketeClone = Instantiate(rakete, transform.position, transform.rotation) as GameObject;
            raketeClone.GetComponent<BoxCollider>().enabled = true;
            raketeClone.GetComponent<BoxCollider>().isTrigger = true;
            raketeClone.GetComponent<TrailRenderer>().enabled = true;
            raketeClone.SetActive(true);
            //add Script
            raketeClone.AddComponent<Bullet>();
            //call BulletScript
            Bullet script = raketeClone.gameObject.GetComponent<Bullet>();
            if (script != null)
            {
                script.changeBullet(dmg, range, impactForce, impactEffect, activeWeapon, Explosion);
            }
            //move forwards
            Vector3 vec = Vector3.forward * bulletSpeed;
            raketeClone.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.transform.TransformDirection(vec);
            //Get the bullet's rigid body component and set its position and rotation equal to that of the spawnPoint
            raketeClone.transform.rotation = bulletSpawnPoint.transform.rotation;
            raketeClone.transform.position = bulletSpawnPoint.transform.position;

            //reload
            GetComponent<ReloadScript>().reload(rakete);
        }
        else
        {
            nextTimeToFire = Time.time + 1f / fireRate;

            //play Shoot animation
            flash.Play();
            if(activeWeapon.Contains("Upgrade")) GetComponent<AudioSource>().PlayOneShot(upgradedShootSound);
            else GetComponent<AudioSource>().PlayOneShot(shootSound);

            if (activeWeapon == "Bennelli_M4" || activeWeapon == "Bennelli_M4Upgrade")
            {
                spawnBullets(7);
            }
            else
            {
                spawnBullets(1);
            }

            //Clone emty bullets
            GameObject emtyBulletClone = Instantiate(emtyBullet, transform.position, transform.rotation) as GameObject;
            emtyBulletClone.transform.localScale = new Vector3(bulletSize / 30, bulletSize / 30, bulletSize / 30);
            //Get the bullet's rigid body component and set its position and rotation equal to that of the spawnPoint
            emtyBulletClone.transform.rotation = emtyBulletSpawnPoint.transform.rotation;
            emtyBulletClone.transform.position = emtyBulletSpawnPoint.transform.position;
            //move forwards 
            Vector3 randomVector = new Vector3(Random.Range(0.4f, 1f), Random.Range(0f, 1f), 0) * emtyBulletSpeed;
            emtyBulletClone.GetComponent<Rigidbody>().velocity = emtyBulletSpawnPoint.transform.TransformDirection(randomVector);
            Destroy(emtyBulletClone, 1f);
        }
    }

    private void spawnBullets(int bulletAmount)
    {
        for (int i = 1; i <= bulletAmount; i++)
        {
            //Clone bullets
            GameObject bulletClone = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
            bulletClone.transform.localScale = new Vector3(bulletSize / 30, bulletSize / 30, bulletSize / 30);
            //add Script
            bulletClone.AddComponent<Bullet>();
            //call BulletScript
            Bullet script = bulletClone.gameObject.GetComponent<Bullet>();
            if (script != null)
            {
                script.changeBullet(dmg, range, impactForce, impactEffect, activeWeapon, Explosion);
            }
            //move forwards
            Vector3 vec = Vector3.forward * bulletSpeed;
            if (bulletAmount > 1) vec = (Vector3.forward + new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f))) * bulletSpeed;
            bulletClone.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.transform.TransformDirection(vec);
            //Get the bullet's rigid body component and set its position and rotation equal to that of the spawnPoint
            bulletClone.transform.rotation = bulletSpawnPoint.transform.rotation;
            bulletClone.transform.position = bulletSpawnPoint.transform.position;
        }
    }

    public void activateDoubleTap (bool activate)
    {
        doubleTap = activate;
    }

    public void changeWeaponStats()
    {
        m_CurrentInteractable = GetComponent<Hand>().getGrabbeldObject();
        if (m_CurrentInteractable)
        {
            activeWeapon = m_CurrentInteractable.transform.name;
            bulletSpawnPoint = m_CurrentInteractable.gameObject.transform.Find("BulletSpawner").gameObject;
            emtyBulletSpawnPoint = m_CurrentInteractable.gameObject.transform.Find("EmptyBulletSpawner").gameObject;
            flash = m_CurrentInteractable.gameObject.transform.Find("MuzzleFlash").GetComponent<ParticleSystem>();
        } 
        
        if(activeWeapon.Contains("Upgrade"))
        {
            dmg = dmg * 1.4f;
            impactForce = impactForce * 1.4f;
            fireRate = fireRate * 1.4f;
            range = range * 1.4f;
        }
        else
        {
            switch (activeWeapon)
            {
                case "Bennelli_M4":
                    {
                        dmg = 40f;
                        impactForce = 20f;
                        fireRate = 1f;
                        range = 20f;
                        bulletSize = 16f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "AK74":
                    {
                        dmg = 25f;
                        impactForce = 20f;
                        fireRate = 10f;
                        range = 100f;
                        bulletSize = 10f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "M4_8":
                    {
                        dmg = 20f;
                        impactForce = 10f;
                        fireRate = 15f;
                        range = 100f;
                        bulletSize = 10f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "M107":
                    {
                        dmg = 150f;
                        impactForce = 80f;
                        fireRate = 1f;
                        range = 200f;
                        bulletSize = 18f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "M249":
                    {
                        dmg = 20f;
                        impactForce = 10f;
                        fireRate = 20f;
                        range = 100f;
                        bulletSize = 9f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "M1911":
                    {
                        dmg = 10f;
                        impactForce = 10f;
                        fireRate = 4f;
                        range = 100f;
                        bulletSize = 9f;
                        bulletSpeed = 50f;
                        break;
                    }
                case "RPG7":
                    {
                        dmg = 300f;
                        impactForce = 150f;
                        fireRate = 0.5f;
                        range = 600f;
                        bulletSize = 60f;
                        bulletSpeed = 30f;
                        break;
                    }
                case "Uzi":
                    {
                        dmg = 15f;
                        impactForce = 10f;
                        fireRate = 20f;
                        range = 100f;
                        bulletSize = 7f;
                        bulletSpeed = 50f;
                        break;
                    }
            }
        }

        if (doubleTap)
        {
            fireRate = fireRate * 2;
        }
    }
}
