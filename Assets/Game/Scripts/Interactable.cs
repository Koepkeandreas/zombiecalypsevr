﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Interactable : MonoBehaviour
{
    [HideInInspector]
    public Hand m_ActiveHand = null;
    [SerializeField]
    protected bool m_snapOrientation = false;
    [SerializeField]
    protected bool m_snapPosition = false;
    [SerializeField]
    protected Transform m_snapOffset;

    public bool snapPosition
    {
        get { return m_snapPosition; }
    }

    public bool snapOrientation
    {
        get { return m_snapOrientation; }
    }

    public Transform snapOffset
    {
        get { return m_snapOffset; }
    }
}
